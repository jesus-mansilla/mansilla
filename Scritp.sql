/*
SQLyog Community v11.42 (32 bit)
MySQL - 5.5.24-log : Database - ventalibro
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ventalibro` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ventalibro`;

/*Table structure for table `compra` */

DROP TABLE IF EXISTS `compra`;

CREATE TABLE `compra` (
  `idCompra` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idUsuario` int(10) NOT NULL,
  `idLibro` int(10) NOT NULL,
  `fechaVenta` datetime DEFAULT NULL,
  `descuento` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  PRIMARY KEY (`idCompra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compra` */

/*Table structure for table `libro` */

DROP TABLE IF EXISTS `libro`;

CREATE TABLE `libro` (
  `idLibro` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombreLibro` varchar(256) NOT NULL,
  `autor` varchar(256) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `ISBN` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`idLibro`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `libro` */

insert  into `libro`(`idLibro`,`nombreLibro`,`autor`,`precio`,`ISBN`) values (1,'Los Nazarenos','Jose Milla',45.5,'4-899784-A'),(2,'La Isla Misteriosa ','Julio Verne',55.5,'4-899784-A'),(3,'El Altimista','Paulo Coelho',200,'1-787887-K'),(4,'Harry Poter','Joanne Rowling',500.25,'5-889798-J'),(5,'El Diario de Ana Frank','Ana Frank',12.8,'78-87777-O'),(6,'El Principito ','Antoine Saint ',15,'2-487744-N');

/*Table structure for table `usuario` */

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `idUsuario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(256) NOT NULL,
  `apellido` varchar(256) DEFAULT NULL,
  `login` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `usuario` */

/* Procedure structure for procedure `sp_AutenticarUser` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_AutenticarUser` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_AutenticarUser`(IN _login VARCHAR(128),IN _password VARCHAR(128))
BEGIN
	SELECT idUsuario,nombre,apellido,login,PASSWORD FROM usuario
	WHERE login = _login AND PASSWORD =(_password);
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
