package org.jesusmansilla.db;

import org.hibernate.cfg.Configuration;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Conexion {
	private SessionFactory sesion;
	public static Conexion instancia = null;
	public static Conexion getInstancia(){
		if(instancia == null){
			instancia = new Conexion();
		}
		return instancia;
	}

	public Conexion(){
		
		try{
			 sesion = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		}catch(HibernateException e){
			e.printStackTrace();
		}
	}
	
	public Session getSesion(){
		return sesion.getCurrentSession();
	}
	
	public void agregar(Object registro){
		Session sesion = getSesion();
		Transaction tx = sesion.beginTransaction();
		try{
			sesion.save(registro);
			tx.commit();
		}catch(HibernateException e){
			e.printStackTrace();
			tx.rollback();
		}
	}
	
	

}
