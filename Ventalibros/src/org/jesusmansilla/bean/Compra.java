package org.jesusmansilla.bean;

import java.util.Calendar;
import java.util.Date;


import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

public class Compra {
	
	private int idCompra;
	private Usuario usuario;
	private Libro libro;
	private Date fechaVenta;
	private double descuento;
	private double total;
	
	
	
	public Compra(){
		
	}

	public Compra(int idCompra, Usuario usuario, Libro libro, Date fechaVenta, double descuento, double total) {
		super();
		this.idCompra = idCompra;
		this.usuario = usuario;
		this.libro = libro;
		this.fechaVenta = fechaVenta;
		this.descuento = descuento;
		this.total = total;
	}

	public int getIdCompra() {
		return idCompra;
	}

	public void setIdCompra(int idCompra) {
		this.idCompra = idCompra;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Libro getLibro() {
		return libro;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}

	public Date getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
	
	
	
	
	
	

}
