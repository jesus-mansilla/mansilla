package org.jesusmansilla.bean;

import java.util.HashSet;
import java.util.Set;

public class Libro {
	
	private int idLibro;
	private String nombreLibro;
	private String autor;
	private float precio;
	private String ISBN;
	
	

	private Set<Compra> compra = new HashSet<Compra>();
	
	public Libro(){
		
	}

	public Libro(int idLibro, String nombreLibro, String autor, float precio, String iSBN) {
		super();
		this.idLibro = idLibro;
		this.nombreLibro = nombreLibro;
		this.autor = autor;
		this.precio = precio;
		ISBN = iSBN;
	}

	public int getIdLibro() {
		return idLibro;
	}

	public void setIdLibro(int idLibro) {
		this.idLibro = idLibro;
	}

	public String getNombreLibro() {
		return nombreLibro;
	}

	public void setNombreLibro(String nombreLibro) {
		this.nombreLibro = nombreLibro;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public Set<Compra> getCompra() {
		return compra;
	}

	public void setCompra(Set<Compra> compra) {
		this.compra = compra;
	}
	
	

}