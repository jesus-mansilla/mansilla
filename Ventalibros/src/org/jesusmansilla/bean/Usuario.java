package org.jesusmansilla.bean;

import java.sql.Connection;
import java.util.HashSet;
import java.util.Set;
import java.sql.SQLException;
import java.sql.*;
import org.jesusmansilla.db.Conexion;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Usuario 
{
	private int idUsuario;
    private String nombre,apellido,login,password;
    
    private Set<Compra> compras = new HashSet<Compra>();
    
    public Usuario()
    {
        nombre="";
        apellido="";
        login="";
        password="";
    }      
    
   

	public Usuario(int idUsuario, String nombre, String apellido, String login, String password) {
		super();
		this.idUsuario = idUsuario;
		this.nombre = nombre;
		this.apellido = apellido;
		this.login = login;
		this.password = password;
	}



	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}



	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}


	public Set<Compra> getCompras() {
		return compras;
	}


	public void setCompras(Set<Compra> compras) {
		this.compras = compras;
	}
	

	

}