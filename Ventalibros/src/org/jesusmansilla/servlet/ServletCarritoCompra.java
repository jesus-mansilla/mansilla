package org.jesusmansilla.servlet;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.jesusmansilla.bean.Compra;
import org.jesusmansilla.bean.Libro;
import org.jesusmansilla.bean.Usuario;
import org.hibernate.Query;
import org.jesusmansilla.db.Conexion;
import javax.servlet.http.HttpSession;

 public class ServletCarritoCompra extends HttpServlet {
	 public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
			doPost(peticion,respuesta);
		}
	 
	 public ArrayList<Libro> getLibros(Session sesionEn){
		 Query query = sesionEn.createQuery("from Libro");
		 ArrayList<Libro> librosList = (ArrayList<Libro>) query.list();
		 return librosList;
	 }

	 public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
			RequestDispatcher despachador = null;
			Session sesion = Conexion.getInstancia().getSesion();
			sesion.beginTransaction();
			ArrayList<Libro> listaLibro = getLibros(sesion);
			ArrayList<Libro> listaLibro2 = new ArrayList<Libro>();
			float total = 0;
			try{
	        String[] values=peticion.getParameterValues("libros"); 
	        for (int x=0; x<listaLibro.size(); x++){
	        for(int i=0;i<values.length;i++){
	        	int filtro = Integer.parseInt(values[i]);
	        	if((filtro == listaLibro.get(x).getIdLibro())){
	        		listaLibro2.add(listaLibro.get(x));
	        		total = total + listaLibro.get(x).getPrecio();
	        	}
	       }   
	    }
			}catch(Exception x){
				Set<Libro> index2 = new HashSet<Libro>(listaLibro);
				peticion.setAttribute("ListadoLibro",index2);
				despachador = peticion.getRequestDispatcher("index.jsp");
				despachador.forward(peticion,respuesta);
			}
	        
	    	Set<Libro> carro = new HashSet<Libro>(listaLibro2);
	    	Set<Libro> index = new HashSet<Libro>(listaLibro);
			peticion.setAttribute("ListadoLibro",carro);
			double aprox = Math.round((total-(total*0.10))*100.0)/100.0;
			peticion.setAttribute("total",aprox);
			try{
				String accion = peticion.getParameter("peticion");
				
 			if(accion.equals("1")){
 				if (listaLibro2.size() > 0){ 
 				Date fecha = new Date();
 				Compra compra = new Compra();
 				HttpSession sesionUsuario = peticion.getSession();
 				Usuario usuario = (Usuario)sesionUsuario.getAttribute("usuario");
 				for(int x = 0; x<listaLibro2.size(); x++ ){
 				ServletCompras.getInstancia().getCompra(usuario.getIdUsuario());
 				compra.setLibro(listaLibro2.get(x));
 				compra.setUsuario(usuario);
 				double descuento =  Math.round((listaLibro2.get(x).getPrecio()*0.10)*100.0)/100.0;
 				compra.setDescuento(descuento);
 				compra.setTotal(Math.round((listaLibro2.get(x).getPrecio()-descuento)*100.0)/100.0);
 				compra.setFechaVenta(fecha);
 				Conexion.getInstancia().agregar(compra);
 				peticion.setAttribute("Actualizada", "(Actualizar)");
 				peticion.setAttribute("ListadoLibro",index);
 				despachador = peticion.getRequestDispatcher("index.jsp");
 				}
 				}
			}else if (accion.equals("3")){
				peticion.setAttribute("ListadoLibro",index);
				peticion.setAttribute("Actualizada", "");
				despachador = peticion.getRequestDispatcher("index.jsp");
			}else{
				if (listaLibro2.size() > 0){ 
				peticion.setAttribute("Actualizada", "");
				despachador = peticion.getRequestDispatcher("carritoCompra.jsp");
				}
			}
 			despachador.forward(peticion,respuesta);
			listaLibro.clear();
			}catch(Exception x){
				peticion.setAttribute("Actualizada", "");
				despachador = peticion.getRequestDispatcher("carritoCompra.jsp");
				despachador.forward(peticion,respuesta);
			}
	 }	
}
   