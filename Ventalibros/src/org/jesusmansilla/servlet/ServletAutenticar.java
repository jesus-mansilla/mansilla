package org.jesusmansilla.servlet;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import java.io.IOException;
import java.util.Set;
import java.util.HashSet;

import org.hibernate.Session;
import org.jesusmansilla.bean.Libro;
import org.jesusmansilla.bean.Usuario;
import org.hibernate.Query;
import org.jesusmansilla.db.Conexion;
import javax.servlet.http.HttpSession;

 public class ServletAutenticar extends HttpServlet {
	 public static ServletAutenticar instancia = null;
		public static ServletAutenticar getInstancia(){
			if(instancia == null){
				instancia = new ServletAutenticar();
			}
			return instancia;
		}

	 public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
			doPost(peticion,respuesta);
		}
	 
	 
	 public Set<Libro> getLibros(Session sesionEn){
		 Query query = sesionEn.createQuery("from Libro");
		 Set<Libro> librosList = new HashSet<Libro>(query.list());
		 return librosList;
		 	 
	 }

	 public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
			RequestDispatcher despachador = null;
			Libro libro = new Libro();
			Session sesion = Conexion.getInstancia().getSesion();
			sesion.beginTransaction();
			try{
				Query procedimiento = sesion.getNamedQuery("sp_AutenticarUser");
				procedimiento.setString("_login",peticion.getParameter("txtLogin"));
				procedimiento.setString("_password",peticion.getParameter("txtPassword"));
				Usuario usuario = (Usuario)procedimiento.uniqueResult();
				if(usuario == null & !(peticion.getParameter("txtEntrar").equals("Ir al Inicio"))){
					despachador = peticion.getRequestDispatcher("login.jsp");
				}else{
					HttpSession sesionUsuario = peticion.getSession(true);
					sesionUsuario.setAttribute("usuario",usuario);
					peticion.setAttribute("usuario",usuario);
					Set<Libro> ListadoLibro = getLibros(sesion);	
					peticion.setAttribute("ListadoLibro",ListadoLibro);
					despachador = peticion.getRequestDispatcher("index.jsp");
						}
				sesion.getTransaction().commit();
			}catch(Exception e){
				e.printStackTrace();
				sesion.getTransaction().rollback();
			}
			despachador.forward(peticion,respuesta);
		}
	}
   