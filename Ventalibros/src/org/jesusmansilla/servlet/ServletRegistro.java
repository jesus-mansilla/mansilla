package org.jesusmansilla.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import java.io.IOException;

import org.jesusmansilla.db.Conexion;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jesusmansilla.bean.Usuario;


public class ServletRegistro extends HttpServlet {
	public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
	
	public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException,ServletException{
		RequestDispatcher despachador = null;
		Usuario usuario = new Usuario();
		usuario.setNombre(peticion.getParameter("txtNombre"));
		usuario.setApellido(peticion.getParameter("txtApellido"));
		usuario.setLogin(peticion.getParameter("txtLogin"));
		usuario.setPassword(peticion.getParameter("txtPassword"));
		Conexion.getInstancia().agregar(usuario);
		HttpSession sesionUsuario = null;
		sesionUsuario = peticion.getSession(true);
		sesionUsuario.setAttribute("usuario", usuario);
		peticion.setAttribute("usuario", usuario);	
		despachador = peticion.getRequestDispatcher("login.jsp");
		System.out.println(usuario.getIdUsuario());
		Session sesion = Conexion.getInstancia().getSesion();
		sesion.beginTransaction();
		despachador.forward(peticion, respuesta);
	
	}
}
