<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<link rel="stylesheet" type="text/css" href="f.css"> 
	<h1>Bienvenido ${usuario.nombre} ${usuario.apellido}</h1>
			<input class="button" type="submit" value="cerrar sesion" onclick = "location='http://localhost:8080/VentaLibro/login.jsp'"/>
	<table class="flat-table flat-table-1">
		<thead>
		<th>libro</th>
		<th>fecha</th>
		<th>decuento</th>
		<th>Total</th>
		</thead>
		<tbody>
		<c:forEach  items="${listado}"  var="compra">
				<tr>
				<td>${compra.libro.nombreLibro}</td>
				<td>${compra.fechaVenta}</td>
				<td>${compra.descuento}</td>
				<td>${compra.total}</td>
				
				</tr>
		</c:forEach>
		</tbody>
	</table>
		<ul>
		<a class="button" href="javascript:window.history.back();">&laquo; Catalogo de libros</a>
		</ul>
</body>
</html>