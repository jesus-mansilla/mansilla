<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<link rel="stylesheet" type="text/css" href="f.css"> 
	<h1>Productos del carrito </h1>
		<tr>
			<input class="button" type="submit" value="cerrar sesion" onclick = "location='http://localhost:8080/VentaLibro/login.jsp'"/>
		</tr>
	<form action="ServletCarritoCompra.do" method="post">
	<table class="flat-table flat-table-3">
		<thead>
		<tr>
		<td>nombreLibro</td>
		<td>precio</td>
		<td>Comprar</td>
		<tr/>
		</thead>
		<tbody>
		<c:forEach  items="${ListadoLibro}"  var="libro">
				<tr>
				<td>${libro.nombreLibro}</td>
				<td>${Math.round((libro.precio-(libro.precio*0.10))*100.0)/100.0}</td>
				<td><input id='check-1' type="checkbox" name="libros" value="${libro.idLibro}" checked /></td>
				</tr>
		</c:forEach>
		<td>Total:</td>
		<td>${total}</td>
		<td><input class="button" type="submit" name="names" value="Aceptar"/></td>
		</tbody>
		<td>
			 	<input class="button" id='radio-1' type="radio" name="peticion" value="1" checked="checked">Comprar Selecciones<br>
  		<input class="button" id='radio-1' type="radio" name="peticion" value="2">Actualizar Total y Eliminar Deselecciones<br>
  		<input class="button" id='radio-1' type="radio" name="peticion" value="3">Regresar al Inicio<br>
  		</td>
	</table>
		<ul>
		</ul>
</body>
</html>