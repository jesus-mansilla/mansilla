<%@taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- En caso de que exista una sesion iniciada redirecciono a index.jsp. "NO tiene caso mostrar este formulario cuando hay una sesion iniciada --%>
<t:if test="${sessionScope['sessionUsuario']!=null}">
    <% response.sendRedirect("login.jsp");%>
</t:if>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="f2.css">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       
    </head>
    <body>
        <p style="color: #ff0000">${sessionScope['error']}</p>
        <form action="ServletRegistro.do" method="post">
            <p>Nombre: <input type="text" name=txtNombre></p>
            <p>Apellido: <input type="text" name="txtApellido"></p>
            <p>Usuario: <input type="text" name="txtLogin"></p>
            <p>Clave: <input type="password" name="txtPassword"></p>
            <p><input type="submit" value="Registrar"></p>
            <p><a href="login.jsp">Ya tengo una cuenta</a></p>
        </form>
    </body>
</html>