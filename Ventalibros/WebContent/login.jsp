<%@taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- En caso de que exista una sesion iniciada redirecciono a index.jsp. "NO tiene caso mostrar este formulario cuando hay una sesion iniciada --%>
<t:if test="${sessionScope['sessionUsuario']!=null}">
    <% response.sendRedirect("index.jsp");%>
</t:if>
<!DOCTYPE html> 
<html>
<link rel="stylesheet" href="f2.css">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    </head>
    <body>
        <form action="ServletAutenticar.do" method="post">
              <h1>Iniciar sesion</h1>
            <p> Usuario: <input type="text" name="txtLogin"></p>
            <p> Clave: <input type="password" name="txtPassword"></p>
            <p><input type="submit" value="Login" name="txtEntrar"></p>
            <p><a href="registro.jsp">Crear nueva cuenta de usuario</a></p>
        </form>
    </body>
</html>
 